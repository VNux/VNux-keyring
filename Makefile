V=20210911

PREFIX = /usr/local

install:
	install -dm755 $(DESTDIR)$(PREFIX)/share/pacman/keyrings/
	install -m0644 VNux.gpg $(DESTDIR)$(PREFIX)/share/pacman/keyrings/
	install -m0644 VNux-trusted $(DESTDIR)$(PREFIX)/share/pacman/keyrings/
	install -m0644 VNux-revoked $(DESTDIR)$(PREFIX)/share/pacman/keyrings/

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/share/pacman/keyrings/vnux{.gpg,-trusted,-revoked}
	rmdir -p --ignore-fail-on-non-empty $(DESTDIR)$(PREFIX)/share/pacman/keyrings/

dist:
	git archive --format=tar --prefix=vnux-keyring-$(V)/ $(V) | gzip -9 > vnux-keyring-$(V).tar.gz
	gpg --detach-sign --use-agent vnux-keyring-$(V).tar.gz

.PHONY: install uninstall dist upload
